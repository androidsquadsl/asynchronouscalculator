/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serverforandroidapp;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerForAndroidApp {

    public static double doOp(double op1, double op2, char op) throws Exception {
        switch (op) {
        case '+':
            return op1 + op2;
        case '-':
            return op1 - op2;
        case'*':
            return op1 * op2;
        case '/':
            if (op2 != 0) return op1 / op2;
            else throw new Exception();
        default:
            throw new Exception();
        }
    }
    
    public static void main(String[] args) throws Exception {
        // Example of a distant calculator
        ServerSocket ssock = new ServerSocket(9876);
        
        while (true) { // infinite loop
            Socket comm = ssock.accept();
            System.out.println("connection established");
            
            new Thread(new MyCalculusRunnable(comm)).start();
        }
    }
}


class MyCalculusRunnable implements Runnable {
    private Socket sock;
    
    public MyCalculusRunnable(Socket s) {
        sock = s;
    }
    
    @Override
    public void run() {
        try {
            DataInputStream dis = new DataInputStream(sock.getInputStream());
            DataOutputStream dos = new DataOutputStream(sock.getOutputStream());
            
            // read op1, op2 and the opreation to make
            Double op1 = dis.readDouble();
            char op = dis.readChar(); 
            Double op2 = dis.readDouble();
            
            Double res = ServerForAndroidApp.doOp(op1, op2, op);
            
            // send back result
            dos.writeDouble(res);
            
            // close everything
            dis.close();
            dos.close();
            sock.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
