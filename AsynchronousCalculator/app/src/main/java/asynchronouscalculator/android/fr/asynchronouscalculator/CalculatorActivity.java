package asynchronouscalculator.android.fr.asynchronouscalculator;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class CalculatorActivity extends AppCompatActivity implements View.OnClickListener,
        MyFragment.OnFragmentInteractionListener, ResultFragment.OnFragmentInteractionListener {

    private MyFragment frag1;
    private ResultFragment frag2;

    TextView operation;
    TextView res;

    Socket sock;
    DataInputStream in;
    DataOutputStream out;

    Double first, second, result;
    String op;
    int temp;
    String tempNum;
    // count the number of reverse
    static int counting;

    Boolean operateurPresent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculator);

        frag2 = ResultFragment.newInstance("second fragment", "you");
        getSupportFragmentManager().beginTransaction().add(R.id.myResultFrame, frag2).commit();

        frag1 = MyFragment.newInstance("first fragment", "you");
        getSupportFragmentManager().beginTransaction().add(R.id.myFrame, frag1).commit();

        op = new String("");
        tempNum = new String("");

        // Iitilization
        op = "";
        tempNum = "";
        operateurPresent = false;
        first = null;
        second = null;
        counting = 0;

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try  {
                    // Socket declaration
                    sock = new Socket("10.0.2.2", 9876);

                    // Declaration of the input stream
                    in = new DataInputStream(sock.getInputStream());

                    // Declaration of the output stream
                    out = new DataOutputStream(sock.getOutputStream());

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.my_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.menu_item1:
                // Go to next activity
                Intent intent = new Intent(this, ReviewActivity.class);
                intent.putExtra("ope", operation.getText().toString());
                intent.putExtra("res", res.getText().toString());
                System.out.println(operation.getText());
                System.out.println(res.getText());
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View view) {
        // Initialisation de la variable temporaire
        temp = -1;

        operation = findViewById(R.id.Operation);
        res = findViewById(R.id.Result);

        operation.setText(tempNum);

        switch (view.getId()){
            case R.id.b0:
                temp = 0;
                break;
            case R.id.b1:
                temp = 1;
                break;
            case R.id.b2:
                temp = 2;
                break;
            case R.id.b3:
                temp = 3;
                break;
            case R.id.b4:
                temp = 4;
                break;
            case R.id.b5:
                temp = 5;
                break;
            case R.id.b6:
                temp = 6;
                break;
            case R.id.b7:
                temp = 7;
                break;
            case R.id.b8:
                temp = 8;
                break;
            case R.id.b9:
                temp = 9;
                break;
            case R.id.bmore:
                op = "+";
                break;
            case R.id.bless:
                op = "-";
                break;
            case R.id.bdivide:
                op = "/";
                break;
            case R.id.bmultiply:
                op = "*";
                break;
            case R.id.bequal:
                if(second != null) {

                    Thread thread = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try  {
                                System.out.println(first);
                                out.writeDouble(first);
                                System.out.println(op);
                                out.writeChars(op);
                                System.out.println(second);
                                out.writeDouble(second);
                                out.flush();

                                result = in.readDouble();
                                System.out.println(result);

                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        res.setText("" + result);
                                    }
                                });

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                    thread.start();
                }
                break;
            case R.id.breverseFragments:
                // Reverse both fragments
                if(counting % 2 == 0) {
                    getSupportFragmentManager().beginTransaction().replace(R.id.myFrame, new ResultFragment()).commit();
                    getSupportFragmentManager().beginTransaction().replace(R.id.myResultFrame, new MyFragment()).commit();
                }
                else{
                    getSupportFragmentManager().beginTransaction().replace(R.id.myResultFrame, new ResultFragment()).commit();
                    getSupportFragmentManager().beginTransaction().replace(R.id.myFrame, new MyFragment()).commit();
                }
                counting++;
                break;
        }
        System.out.println("Beginning");
        if(!operateurPresent) {
            // On peut ajouter un opérateur
            System.out.println("No operator yet");
            System.out.println(op);
            if(op == "") {
                System.out.println("Operator empty");
                if(temp != -1) {
                    // Ajouter un premier int
                    System.out.println("Filling first int");
                    tempNum += "" + temp;
                    operation.append("" + temp);
                    System.out.println(tempNum);
                    first = Double.valueOf(tempNum);
                    System.out.println(first);
                }
            }
            else{
                if(first != null) {
                    // On passe au second nombre
                    System.out.println("Operator found");
                    operation.append(op);
                    operateurPresent = true;
                    tempNum = "";
                }
            }
        }
        else{
            System.out.println("Operator exists");
            if(temp != -1) {
                System.out.println("Filling second int");
                tempNum += "" + temp;
                operation.append("" + temp);
                second = Double.valueOf(tempNum);
            }
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}