package asynchronouscalculator.android.fr.asynchronouscalculator;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class ReviewActivity extends AppCompatActivity implements View.OnClickListener {

    EditText e;
    String url;
    String operation, result;
    TextView op, res;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review);
        e = (EditText) findViewById(R.id.webAddress);
        e.setText("http://www.");
        operation = getIntent().getStringExtra("ope");
        result = getIntent().getStringExtra("res");
        op = findViewById(R.id.Operation);
        res = findViewById(R.id.Result);
        op.setText(operation);
        res.setText(result);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.buttonWeb:
                // Go to next activity
                Intent intent = new Intent(this, WebActivity.class);
                url = String.valueOf(e.getText());
                intent.putExtra("url", url);
                startActivity(intent);
                break;
        }
    }
}
